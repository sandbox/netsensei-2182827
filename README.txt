DESCRIPTION

Provides a field which contains your CSS styles. The CSS Inliner formatter is
set on text fields containing HTML. When the text field is displayed, the CSS
styles are applied inline on the elements with the corresponding selectors in
their class attribute.

REQUIREMENTS

Requires composer (getcomposer.org) to install the tijsverkoyen/css-to-inline-styles
library (https://packagist.org/packages/tijsverkoyen/css-to-inline-styles)

INSTALLATION

* Download and install the Drupal module
* Navigate to the module folder (cd /sites/*/modules/contrib/codeinliner)
* execute the 'composer install' command
* Enable the module

USAGE

* Create a new 'CSS Inliner' field on your entity bundle (content type)
* Set the 'CSS inliner' formatter on the text field(s) you want the styles to be applied
* Configure the formatter: select the source CSS field whose styles should be applied
* Navigate to the display of your entity: you should see your HTML output with
inline CSS rules.
